import sys
import matplotlib.pylab as plt
import numpy as np
import os
import re
r = re.compile('(\d+)wolves_(\d+)healers_(\d+)seers_([0-9.]+)intuition\.txt')
caption = sys.argv[1]
outfile = sys.argv[2]
datacol = int(sys.argv[3])
files = sys.argv[4:]

plt.hold(True)

colors = ['red','green','blue','violet','orange','yellow','pink','lime','gray','black','turquoise','brown']
i=0
for path in files:
	m = r.match(os.path.basename(path))
	if m == None:
		print "Invalid file name:", os.path.basename(path)
		sys.exit(1)

	if m.group(1) == '1':
		wolftxt = 'wolf'
	else:
		wolftxt = 'wolves'
	if m.group(2) == '1':
		healertxt = 'healer'
	else:
		healertxt = 'healers'
	if m.group(3) == '1':
		seertxt = 'seer'
	else:
		seertxt = 'seers'

	label = '%s %s, %s %s, %s %s, %s intuition'%(m.group(1),wolftxt,m.group(2),healertxt,m.group(3),seertxt, m.group(4))
	print label
	try:
		data = np.loadtxt(path)
	except IOError:
		print "Bad data file: %s"%path
		continue

	plt.plot(data[:,0],data[:,datacol],label=label,color=colors[i])
	plt.errorbar(data[:,0],data[:,datacol],yerr=data[:,datacol+1],color=colors[i])
	i+=1

plt.xlabel('Players')
if datacol == 1:
	plt.ylim((0,1.0))
	plt.yticks(np.arange(0.0,1.01,0.1))
	plt.ylabel('Human wins')
	plt.axhline(y=0.5,linestyle='--',color='black')
elif datacol == 2:
	plt.ylabel('Game length [turns]')
	plt.yticks(np.arange(data[0,datacol],data[-1,datacol]+1,1))
plt.xticks(np.arange(data[0,0],data[-1,0]+1,1))

plt.title(caption)
plt.legend(loc='best')
plt.savefig(outfile)
plt.show()
