import sys
import random
import logging
import os
import numpy as np
logging.basicConfig(stream=sys.stderr, level=logging.ERROR)

random.seed(123456) # Change this if needed
# Edit these lines to change what rules are tried
# =======
HEALERS = [0, 1] # Number of healers
SEERS = [0, 1] # Number of seers
WOLVES = [1, 2, 3] # Number of wolves
INTUITIONS = [0, 0.01, 0.05, 0.10] # For tuning the model
# =======

HEALERS = [0] # Number of healers
SEERS = [0] # Number of seers
INTUITIONS = [0,0.10,100] # For tuning the model

# Define some types
NORMAL = 0
HEALER = 1
SEER = 2
WOLF = 3 # Wolves need to be the last one

# Pick an element by weighted probability
def weighted_choice(candidates, weights):
	candidates = list(candidates)
	ws = [weights.get(type(x),1) for x in candidates]
	cum = np.array([sum(ws[:i+1]) for i in range(len(ws))])
	cum = cum/cum[-1]
	r = random.random()
	for i,c in enumerate(cum):
		if r <= c:
			return candidates[i]

# Base class for different types of players
class Player:
	pid = -1 # Running number used to identify player (for debugging)
	def __init__(self, intuition, typ):
		self.mtyp = typ
		Player.pid += 1
		self.pid = Player.pid
		self.known = set([self]) # Players know their own class
		self.kill_preferences = [WolfPlayer] # By default players want to kill wolves
		self.save_preferences = [SeerPlayer, HealerPlayer, NormalPlayer, WolfPlayer, Player] # And they want to save the others
		self.intuition = intuition
		
	def die(self):
		# Check if a player is protected
		if not self in HealerPlayer.protected:
			logging.debug("Player %d of type %d died!", self.id(), self.typ())
			Player.players.remove(self)
			return True
		else:
			logging.debug("Player %d of type %s saved by healing!", self.id(), self.typ())
			return False

	def typ(self):
		return self.mtyp

	def id(self):
		return self.pid

	def pick_kill(self, candidates):
		known_candidates = self.knownfrom(candidates)

		# Check if there is anyone we know we want dead
		for typ in self.kill_preferences:
			possible = [x for x in known_candidates if isinstance(x, typ)] # Check which people we known we want to kill
			if possible == []: # No known candidates
				continue # Go to next type
			return random.sample(possible, 1)[0] # Kill one of the baddies

		# Didn't find anyone we wanted to kill
		# Remove friendlies
		possible = candidates
		prev_possible = candidates
		for typ in self.save_preferences:
			possible = possible - set([x for x in known_candidates if isinstance(x, typ)]) # Try to remove everyone of this type
			if possible == set([]): # No one left if we remove them :-( Got to kill one of them then ...
				possible = prev_possible

		weights = dict([(typ, 1+self.intuition) for typ in self.kill_preferences]) # Slightly higher odds of guessing who to kill
		return weighted_choice(possible, weights)

	# Pick a player to protect
	def pick_friend(self, candidates):
		# Try to protect people you know the role of
		known_candidates = self.knownfrom(candidates)
		for typ in self.save_preferences:
			# TODO: Make configurable if healer is allowed to pick self
			possible = [x for x in known_candidates if isinstance(x, typ)]
			if possible != []:
				candidate = random.sample(possible, 1)[0]
				return candidate

		weights = dict([(typ, 1+self.intuition) for typ in self.save_preferences]) # Slightly higher odds of guessing who to save
		return weighted_choice(possible, weights)

	def cast_vote(self, candidates):
		candidate = self.pick_kill(candidates)
		logging.debug('Player %d of type %d votes on %d of type %d', self.id(), self.typ(), candidate.id(), candidate.typ())
		return candidate

	def learn_player(self, player):
		logging.debug('Player %s found out that player %s is of type %s', self.id(), player.id(), player.typ())
		self.known.add(player)

	# Check which other players the current player knows the type of
	def knownfrom(self, players):
		return list(players.intersection(self.known))

class NormalPlayer(Player):
	def __init__(self, intuition):
		Player.__init__(self, intuition, NORMAL)

class WolfPlayer(Player):
	@staticmethod
	# Wolves cooperate in killing so make this method static
	def kill_someone(candidates):
		wolves = [x for x in Player.players if isinstance(x, WolfPlayer)]
		tokill = vote(wolves, set(candidates) - set(wolves)) # Private vote among wolves on who to kill
		logging.debug("Wolves chose target %d of type %s", tokill.id(), tokill.typ())

	def __init__(self, intuition):
		Player.__init__(self, intuition, WOLF)
		self.kill_preferences = [SeerPlayer, HealerPlayer, NormalPlayer]
		self.save_preferences = [WolfPlayer]
		self.weights = dict([(typ, 1+intuition) for typ in self.kill_preferences])

	def learn_player(self, player):
		logging.debug('Wolf %s found out that player %s is of type %s', self.id(), player.id(), player.typ())
		# If one wolf learns something, all of them learn it
		for wolf in [x for x in Player.players if isinstance(x, WolfPlayer)]:
			wolf.known.add(player)

class SeerPlayer(Player):
	def __init__(self, intuition):
		Player.__init__(self, intuition, SEER)

	# Seers can find out if someone is a wolf
	def detect(self, candidates):
		unknown = candidates.difference(self.known) # Which players do the seer not know about yet?
		if unknown == set([]): # Already knows about everyone
			return

		weights = dict([(typ, 1+intuition) for typ in self.kill_preferences]) # Slightly higher odds for wanting to guess a baddie
		guess = weighted_choice(unknown, weights)
		self.learn_player(guess)

class HealerPlayer(Player):
	protected = set([]) # Dying players check here if they are protected. Should be cleared after wolf attacks.
	def __init__(self, intuition):
		Player.__init__(self, intuition, HEALER)

	# Healers can protect someone from attacks
	def protect_someone(self, candidates):
		player = self.pick_friend(candidates)
		logging.debug("Healer %d chose to protect player %d of type %s", self.id(), player.id(), player.typ())
		HealerPlayer.protected.add(player)

def alive(players):
	alive = [0, 0, 0, 0]
	for player in players:
		alive[player.typ()] += 1
	return alive

def setup_game(nplayer, nwolf, nhealer, nseer, intuition):
	# Set up players
	nnormal = nplayer - nhealer - nwolf - nseer
	players = set([])
	Player.players = players
	for i in xrange(nwolf):
		players.add(WolfPlayer(intuition))
	for i in xrange(nnormal):
		players.add(NormalPlayer(intuition))
	for i in xrange(nhealer):
		players.add(HealerPlayer(intuition))
	for i in xrange(nseer):
		players.add(SeerPlayer(intuition))

	# Make sure wolves know about each other
	for wolf in [x for x in players if isinstance(x, WolfPlayer)]:
		wolf.learn_player(wolf) # When a wolf learns about itself, all the others allso do it automatically

	logging.debug('Players: %s', [(x.id(), x.typ()) for x in players])

	return players

def play(nplayer, nwolf, nhealer, nseer, intuition):
	rounds = 0

	players = setup_game(nplayer, nwolf, nhealer, nseer, intuition)

	while True: # Let's play
		rounds += 1
		logging.debug("Starting round %d", rounds)

		# Count how many are still alive (for debugging)
		survivors = alive(players)
		logging.debug("Alive: %s (%s)", [x.id() for x in players], alive)

		# Healers do their thing
		for healer in [x for x in players if isinstance(x, HealerPlayer)]:
			healer.protect_someone(players)

		# Seers do their thing
		for seer in [x for x in players if isinstance(x, SeerPlayer)]:
			seer.detect(players)

		# Wolves do their thing
		# Dead players are removed from the players list automatically, no need to do it here
		WolfPlayer.kill_someone(players)

		winner = check_victory(players)
		if winner != None:
			return (winner, rounds)

		HealerPlayer.protected = set([]) # Clear healed players

		# It's voting time!
		tolynch = vote(players, players)
		tolynch.die() # Make sure healed players are cleared before calling this or they will be saved!

		winner = check_victory(players)
		if winner != None:
			return (winner, rounds)

def tally_votes(votes):
	tally = {}
	for suspect in votes.values():
		tally[suspect] = tally.get(suspect, 0) + 1
	return tally

def vote(voters, candidates):
	votes = {}

	for player in voters: # Everyone gets a vote
		votes[player] = player.cast_vote(candidates)
	tally = tally_votes(votes)

	logging.debug("Tally: %s", [(x.id(), value) for (x, value) in tally.items()])

	highest = max(tally.values())
	top_suspects = set([key for (key, value) in tally.items() if value == highest]) # List top candidates that got the highest score
	logging.debug('End of voting round: %s (%d votes)', [x.id() for x in top_suspects], highest)

	if len(top_suspects) > 1: # Shared position for top suspect, need another round
		voters = [x for x in voters if votes[x] not in top_suspects] # Let those who voted on non-winners vote on the top suspects
		if voters == []: # Everyone voted on on a winner
			logging.debug('Not possible to choose winner by vote')
			tolynch = random.sample(top_suspects, 1)[0] # Pick a winner at random
		else:
			logging.debug('Doing another voting round with %s', [x.id() for x in voters])
			tolynch = vote(voters, top_suspects)
	else:
		tolynch = top_suspects.pop()

	logging.debug("Lynching player %d of type %s", tolynch.id(), tolynch.typ())
	return tolynch

def check_victory(players):
	survivors = alive(players)
	if survivors[3] == 0:
		logging.debug("Humans wins!")
		return 1
	elif sum(survivors[0:WOLF]) <= survivors[3]: # Wolves win if they equal humans in number
		logging.debug("Wolves wins!")
		return 0
	else:
		return None

def usage():
	logging.error('Usage: %s number_of_games_to_run players1 [players2 ...]',sys.argv[0])
	logging.error('e.g. "%s 1000 5 6 7" to run 1000 games for each rule type with 5 6 and 7 players',sys.argv[0])

def runtest():
	if len(sys.argv) < 3:
		usage()
		sys.exit(1)
	try:
		N = int(sys.argv[1])
	except ValueError:
		logging.error('Invalid number of games to run.')
		usage()
		sys.exit(1)
	try:
		PLAYERS = [int(x) for x in sys.argv[2:]]
	except:
		logging.error('Invalid player count.')
		usage()
		sys.exit(1)

	outdir = 'stats'
	try:
		os.mkdir(outdir)
	except OSError:
		pass

	print '# intuition, players, human_wins, wolves, healers, seers'
	# Loop over test space
	for intuition in INTUITIONS:
		best = {}
		for nhealer in HEALERS:
			for nseer in SEERS:
				for nwolf in WOLVES:
					f = open(os.path.join(outdir, '%dwolves_%dhealers_%dseers_%.2fintuition.txt'%(nwolf, nhealer, nseer, intuition)), 'w')
					print >>f, '# players, avg_human_wins, avg_human_wins_stddev, avg_rounds, avg_rounds_stddev'
					for nplayer in PLAYERS:
						winners = np.empty(N)
						rounds = np.empty(N)
						logging.info('Running %d games with %d players (%d wolves, %d healers %d seers, intuition %.2f)', N, nplayer, nwolf, nhealer, nseer, intuition)
						for loop in xrange(N):
							winners[loop], rounds[loop] = play(nplayer, nwolf, nhealer, nseer, intuition)
						wins = np.mean(winners)
						print >> f, nplayer, wins, wins/np.sqrt(N), np.mean(rounds), np.std(rounds)/np.sqrt(N)

						# Find the most balanced rules
						curbest = best.get(nplayer,[1])[0]
						if abs(wins-0.5) < abs(0.5-curbest):
							best[nplayer] = [wins, nwolf, nhealer, nseer]
					f.close()
		for k,v in best.items():
			print intuition, k, v[0],v[1],v[2],v[3]

if __name__ == '__main__':
	runtest()
