mkdir -p $1
python plot.py "Impact of wolves" $1/wolves.png 1 stats/*0healers_0seers_0.00intuition.txt
python plot.py "Impact of special players, 1 wolf" $1/1wolf.png 1 stats/1wolves*_0.00intuition.txt
python plot.py "Impact of special players, 2 wolves" $1/2wolves.png 1 stats/2wolves*_0.00intuition.txt
python plot.py "Impact of special players, 3 wolves" $1/3wolves.png 1 stats/3wolves*_0.00intuition.txt
python plot.py "Impact of intuition, 1 wolves" $1/1wolf_int.png 1 stats/1wolves_0healers_0seers_*.txt
python plot.py "Impact of intuition, 2 wolves" $1/2wolves_int.png 1 stats/2wolves_0healers_0seers_*.txt
python plot.py "Impact of intuition, 3 wolves" $1/3wolves_int.png 1 stats/3wolves_0healers_0seers_*.txt
python plot.py "Game length" $1/lengths.png 3 stats/*0healers_0seers_0.00intuition.txt
